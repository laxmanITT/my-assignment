//use default value for all input in Login
function defaultLogin()
{
    document.getElementById("lemailerror").innerHTML = "  "
    document.getElementById("lpassworderror").innerHTML = ""; 
    document.getElementById("lgpassword").style.border = "1px solid white";
    document.getElementById("lgemail").style.border = "1px solid white";  

}

//hide function is use for hidden the login form
function hideLogin()
{
    document.getElementById("main").style.visibility= "hidden";
    defaultLogin();
    document.getElementById("lemail").value="";
    document.getElementById("lpassword").value=""; 
    document.getElementById("main1").style.visibility="visible";
}


// velidate    login information
function velidateLogin() {
    defaultLogin(); 
    var email = document.getElementById("lemail").value;
    var password = document.getElementById("lpassword").value;

    if (!isEmailValid(email)) {
        document.getElementById("lemailerror").innerHTML = "  Not a valid e-mail address or empty ";
        document.getElementById("lgemail").style.border = "1px solid red";
        return;
    }                       


    if (!isPasswordValid(password)) {
        document.getElementById("lpassworderror").innerHTML = "Must be one number and one special character <br> Must be password lenth 6 .";
        document.getElementById("lgpassword").style.border = "1px solid red";
        return;
    }


    userLogin(email,password);
}

//user login
function userLogin(email,password)
{
    defaultLogin();
    user=JSON.parse(localStorage.getItem(email));
    if (user!=null)
    {
        if(user.email==email)
        {
            if(user.password==password)
            {


                window.location = 'dashboard.html?username='+user.email;

            }
            else
            {
                document.getElementById("lgpassword").style.border = "1px solid red";
                document.getElementById("lpassworderror").innerHTML = "your password is wrong";     
            }


        }
        else
        {
            document.getElementById("lemailerror").innerHTML = "Your Email id is not register";    
            document.getElementById("lgemail").style.border = "1px solid red";  
        }    


    }
    else
    {
        document.getElementById("lemailerror").innerHTML = "Your Email id is not register";    
        document.getElementById("lgemail").style.border = "1px solid red";  
    }

}