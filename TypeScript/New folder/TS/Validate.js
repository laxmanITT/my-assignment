//validate  Email
function isEmailValid(email)
{
    var re = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;

    return re.test(email);



}

// validate Password
function isPasswordValid(password)
{
    var patten1 =  /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
    return patten1.test(password);

}


// validate  Name
function isNameValid(name)
{
    var patten = /^[a-z,. ]+$/i; 
    return  patten.test(name);


}

function   isPhoneValid(phone)
{

    var re = /^[0-9]{10,}$/;

    return re.test(phone);



}