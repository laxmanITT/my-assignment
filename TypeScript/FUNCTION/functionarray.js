function sumMatrix(matrix) {
    var sum = 0;
    var index;
    for (index = 0; index < matrix.length; index++) {
        sum = sum + matrix[index];
    }

    return sum;
}

var x = sumMatrix([5, 6, 8, 4, 6]);
