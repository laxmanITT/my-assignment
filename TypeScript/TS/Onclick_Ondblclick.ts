class Onclick_Ondblclick  
{  
    Onclick()  
    {  
        alert("Fire Onclick event");  
    }  
    Ondblclick()  
    {  
        alert("Fire Ondblclick event");       
    }  
}  
   
window.onload = () =>  
{  
    var obj = new Onclick_Ondblclick();  
    var bttnclick = document.getElementById("onclick");  
    var bttndblclick = document.getElementById("ondblclick");  
    bttnclick.onclick = function ()  
    {  
        obj.Onclick();  
    }  
    bttndblclick.ondblclick = function ()  
    {  
        obj.Ondblclick();  
    }  
};  