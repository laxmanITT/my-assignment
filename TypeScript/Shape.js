var shape = {
    length: 12,
    width: 10,
    radius: 15,
    circleArea: function (radius) {
        return (radius * radius);
    },
    rectangularArea: function (length, width) {
        return (length * width);
    }
};
console.log("AREA OF CIRCLE" + shape.circleArea);
console.log("AREA OF RECTANGULAR" + shape.circleArea);
