///  typescript@2.0.<reference path = "Shape.ts"/>
///  typescript@2.0.<reference path = "Circle.ts"/>
///  typescript@2.0.<reference path = "Rectangular.ts"/>
function areaAllShapes(shape) {
    shape.circleArea();
    shape.rectangularArea();
}

areaAllShapes(new Drawing.Shape());
